﻿using System;
using System.Drawing;
using System.Windows.Forms;
using static System.Math;

namespace DiveIntoTheFractal
{
    public partial class MainForm : Form
    {
        public static Fractal currentFractal;
        private bool _allSettings = false;

        public MainForm()
        {
            // Saves the form from flicker.
            DoubleBuffered = true;
            InitializeComponent();
        }

        private void TreeButton_Click(object sender, EventArgs e)
        {
            currentFractal = new TreeFractal();
            ShowSettings();
            drawButton.Text = "Other settings";
        }

        private void KochButton_Click(object sender, EventArgs e)
        {
            currentFractal = new KochFractal();
            ShowSettings();
        }

        private void CarpetButton_Click(object sender, EventArgs e)
        {
            currentFractal = new CarpetFractal();
            ShowSettings();
        }

        private void TriangleButton_Click(object sender, EventArgs e)
        {
            currentFractal = new TriangleFractal();
            ShowSettings();
        }

        private void CuntorButton_Click(object sender, EventArgs e)
        {
            currentFractal = new CuntorFractal();
            ShowSettings();
            drawButton.Text = "Other settings";
        }

        private void MandelbrotButton_Click(object sender, EventArgs e)
        {
            currentFractal = new MandelbrotFractal();
            ShowSettings();
        }

        private void BackButton_Click(object sender, EventArgs e)
        {
            try
            {
                FormBorderStyle = FormBorderStyle.FixedDialog;
                homeButton.Visible = false;
                choseLabel.Text = "Chose the type of the fractal:";
                treeButton.Visible = true;
                kochButton.Visible = true;
                carpetButton.Visible = true;
                triangleButton.Visible = true;
                cuntorButton.Visible = true;
                mandelbrotButton.Visible = true;
                lowStepsPicture.Visible = false;
                maxStepsPicture.Visible = false;
                gradientBox.Visible = false;
                stepsBar.Visible = false;
                firstColorButton.Visible = false;
                endColorButton.Visible = false;
                firstColorLabel.Visible = false;
                endColorLabel.Visible = false;
                stepsLabel.Visible = false;
                gradientLabel.Visible = false;
                drawButton.Visible = false;
                fractalBox.Visible = false;
                drawButton.Text = "Draw!";
                firstAngleBar.Visible = false;
                firstAngleBox.Visible = false;
                firstAngleLabel.Visible = false;
                secondAngleBar.Visible = false;
                secondAngleBox.Visible = false;
                secondAngleLabel.Visible = false;
                fractalBox.Visible = false;
                _allSettings = false;
                stepsLabel.Text = "Steps count";
                stepsBar.Minimum = 1;
                stepsBar.Maximum = 20;
            }
            catch (Exception someException)
            {
                MessageBox.Show(
                    "Error!",
                    someException.Message,
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Information,
                    MessageBoxDefaultButton.Button1,
                    MessageBoxOptions.DefaultDesktopOnly);
            }
        }

        private void ShowSettings()
        {
            try
            {
                choseLabel.Text = "Set some settings:";
                treeButton.Visible = false;
                kochButton.Visible = false;
                carpetButton.Visible = false;
                triangleButton.Visible = false;
                cuntorButton.Visible = false;
                mandelbrotButton.Visible = false;
                firstColorDiolog.Color = Color.Fuchsia;
                endColorDiolog.Color = Color.Fuchsia;
                FillGradientBox();
                homeButton.Visible = true;
                lowStepsPicture.Visible = true;
                maxStepsPicture.Visible = true;
                gradientBox.Visible = true;
                stepsBar.Visible = true;
                firstColorButton.Visible = true;
                endColorButton.Visible = true;
                firstColorLabel.Visible = true;
                endColorLabel.Visible = true;
                gradientLabel.Visible = true;
                stepsLabel.Visible = true;
                drawButton.Visible = true;
            }
            catch (Exception someException)
            {
                MessageBox.Show(
                    "Error!",
                    someException.Message,
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Information,
                    MessageBoxDefaultButton.Button1,
                    MessageBoxOptions.DefaultDesktopOnly);
            }
        }

        private void FirstColorButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (firstColorDiolog.ShowDialog() == DialogResult.Cancel)
                {
                    return;
                }
                firstColorButton.BackColor = firstColorDiolog.Color;
                FillGradientBox();
            }
            catch (Exception someException)
            {
                MessageBox.Show(
                    "Error!",
                    someException.Message,
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Information,
                    MessageBoxDefaultButton.Button1,
                    MessageBoxOptions.DefaultDesktopOnly);
            }
        }

        private void FillGradientBox()
        {
            try
            {
                Graphics gradientGraphics = gradientBox.CreateGraphics();
                var firstColor = Color.FromArgb(128, firstColorDiolog.Color);
                var endColor = Color.FromArgb(128, endColorDiolog.Color);
                int width = gradientBox.Width;
                for (int lineIndex = 0; lineIndex < width; ++lineIndex)
                {
                    var firstPen = new Pen(Color.FromArgb((int)(255 - (double)lineIndex / width * 255), firstColor));
                    gradientGraphics.DrawLine(firstPen, new Point(lineIndex, 0), new Point(lineIndex, gradientBox.Height));
                    var endPen = new Pen(Color.FromArgb((int)((double)lineIndex / width * 255), endColor));
                    gradientGraphics.DrawLine(endPen, new Point(lineIndex, 0), new Point(lineIndex, gradientBox.Height));
                }
            }
            catch (Exception someException)
            {
                MessageBox.Show(
                    "Error!",
                    someException.Message,
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Information,
                    MessageBoxDefaultButton.Button1,
                    MessageBoxOptions.DefaultDesktopOnly);
            }
        }

        private void EndColorButton_Click(object sender, EventArgs e)
        {
            if (endColorDiolog.ShowDialog() == DialogResult.Cancel)
            {
                return;
            }
            endColorButton.BackColor = endColorDiolog.Color;
            FillGradientBox();
        }

        private void DrawButton_Click(object sender, EventArgs e)
        {
            try
            {
                if ((currentFractal is TreeFractal || currentFractal is CuntorFractal) && !_allSettings)
                {
                    ShowExtraSettings();
                }
                else
                {
                    currentFractal.firstColor = firstColorDiolog.Color;
                    currentFractal.endColor = endColorDiolog.Color;
                    if (currentFractal is TreeFractal)
                    {
                        ((TreeFractal)currentFractal).segmentCoefficient = (double)stepsBar.Value / 150;
                        ((TreeFractal)currentFractal).firstAngle = (double)firstAngleBar.Value / (firstAngleBar.Maximum + 1) * (PI / 2);
                        ((TreeFractal)currentFractal).secondAngle = (double)secondAngleBar.Value / (secondAngleBar.Maximum + 1) * (PI / 2);
                    }
                    else if (currentFractal is CuntorFractal)
                    {
                        ((CuntorFractal)currentFractal).segmentDistance = stepsBar.Value;
                    }
                    else
                    {
                        currentFractal.stepsCount = stepsBar.Value;
                    }
                    fractalBox.Visible = true;
                    lowStepsPicture.Visible = false;
                    maxStepsPicture.Visible = false;
                    gradientBox.Visible = false;
                    stepsBar.Visible = false;
                    firstColorButton.Visible = false;
                    endColorButton.Visible = false;
                    firstColorLabel.Visible = false;
                    endColorLabel.Visible = false;
                    gradientLabel.Visible = false;
                    stepsLabel.Visible = false;
                    drawButton.Visible = false;
                    firstAngleBar.Visible = false;
                    firstAngleBox.Visible = false;
                    secondAngleBar.Visible = false;
                    secondAngleBox.Visible = false;
                    firstAngleLabel.Visible = false;
                    secondAngleLabel.Visible = false;
                    Bitmap map = new Bitmap(fractalBox.Width, fractalBox.Height);
                    fractalBox.Image = map;
                    Graphics fractalGraphics = Graphics.FromImage(map);
                    fractalGraphics.Clear(Color.White);
                    currentFractal.fractalGraphics = fractalGraphics;
                    currentFractal.fractalBox = fractalBox;
                    currentFractal.Draw();
                    FormBorderStyle = FormBorderStyle.SizableToolWindow;
                }
            }
            catch (Exception someException)
            {
                MessageBox.Show(
                    "Error!",
                    someException.Message,
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Information,
                    MessageBoxDefaultButton.Button1,
                    MessageBoxOptions.DefaultDesktopOnly);
            }
        }

        private void ShowExtraSettings()
        {
            try
            {

                if (currentFractal is TreeFractal)
                {
                    currentFractal.firstColor = firstColorDiolog.Color;
                    currentFractal.endColor = endColorDiolog.Color;
                    currentFractal.stepsCount = stepsBar.Value;
                    stepsBar.Maximum = 100;
                    stepsBar.Minimum = 50;
                    stepsLabel.Text = "Segment coefficient";
                    // Изменение картинок
                    firstAngleBar.Visible = true;
                    firstAngleBox.Visible = true;
                    firstAngleLabel.Visible = true;
                    secondAngleBar.Visible = true;
                    secondAngleBox.Visible = true;
                    secondAngleLabel.Visible = true;
                    firstColorButton.Visible = false;
                    firstColorLabel.Visible = false;
                    gradientBox.Visible = false;
                    gradientLabel.Visible = false;
                    endColorButton.Visible = false;
                    endColorLabel.Visible = false;
                }
                else if (currentFractal is CuntorFractal)
                {
                    currentFractal.stepsCount = stepsBar.Value;
                    stepsBar.Maximum = 100;
                    stepsBar.Minimum = 5;
                    stepsLabel.Text = "Segment distance";
                }
                drawButton.Text = "Draw!";
                _allSettings = true;
            }
            catch (Exception someException)
            {
                MessageBox.Show(
                    "Error!",
                    someException.Message,
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Information,
                    MessageBoxDefaultButton.Button1,
                    MessageBoxOptions.DefaultDesktopOnly);
            }
        }

        private void MainFormResize(object sender, EventArgs e)
        {
            try
            {
                fractalBox.Width = Size.Width;
                fractalBox.Height = Size.Height;
                Bitmap map = new Bitmap(fractalBox.Width, fractalBox.Height);
                fractalBox.Image = map;
                Graphics fractalGraphics = Graphics.FromImage(map);
                fractalGraphics.Clear(Color.White);
                currentFractal.fractalGraphics = fractalGraphics;
                currentFractal.fractalBox = fractalBox;
                currentFractal.Draw();
            }
            catch (Exception someException)
            {
                MessageBox.Show(
                    "Error!",
                    someException.Message,
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Information,
                    MessageBoxDefaultButton.Button1,
                    MessageBoxOptions.DefaultDesktopOnly);
            }
        }
    }
}
