﻿namespace DiveIntoTheFractal
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.firstLabel = new System.Windows.Forms.Label();
            this.choseLabel = new System.Windows.Forms.Label();
            this.treeButton = new System.Windows.Forms.Button();
            this.kochButton = new System.Windows.Forms.Button();
            this.carpetButton = new System.Windows.Forms.Button();
            this.triangleButton = new System.Windows.Forms.Button();
            this.cuntorButton = new System.Windows.Forms.Button();
            this.mandelbrotButton = new System.Windows.Forms.Button();
            this.homeButton = new System.Windows.Forms.Button();
            this.lowStepsPicture = new System.Windows.Forms.PictureBox();
            this.maxStepsPicture = new System.Windows.Forms.PictureBox();
            this.firstColorDiolog = new System.Windows.Forms.ColorDialog();
            this.endColorDiolog = new System.Windows.Forms.ColorDialog();
            this.stepsBar = new System.Windows.Forms.TrackBar();
            this.gradientBox = new System.Windows.Forms.PictureBox();
            this.firstColorButton = new System.Windows.Forms.Button();
            this.endColorButton = new System.Windows.Forms.Button();
            this.firstColorLabel = new System.Windows.Forms.Label();
            this.endColorLabel = new System.Windows.Forms.Label();
            this.gradientLabel = new System.Windows.Forms.Label();
            this.stepsLabel = new System.Windows.Forms.Label();
            this.drawButton = new System.Windows.Forms.Button();
            this.firstAngleBox = new System.Windows.Forms.PictureBox();
            this.secondAngleBox = new System.Windows.Forms.PictureBox();
            this.firstAngleBar = new System.Windows.Forms.TrackBar();
            this.secondAngleBar = new System.Windows.Forms.TrackBar();
            this.firstAngleLabel = new System.Windows.Forms.Label();
            this.secondAngleLabel = new System.Windows.Forms.Label();
            this.fractalBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.lowStepsPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxStepsPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stepsBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gradientBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstAngleBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondAngleBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstAngleBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondAngleBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fractalBox)).BeginInit();
            this.SuspendLayout();
            // 
            // firstLabel
            // 
            this.firstLabel.BackColor = System.Drawing.Color.Transparent;
            this.firstLabel.Font = new System.Drawing.Font("Yu Gothic", 60F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.firstLabel.ForeColor = System.Drawing.Color.Fuchsia;
            this.firstLabel.Location = new System.Drawing.Point(12, 9);
            this.firstLabel.Name = "firstLabel";
            this.firstLabel.Size = new System.Drawing.Size(1060, 89);
            this.firstLabel.TabIndex = 0;
            this.firstLabel.Text = "Dive into the fractal...";
            this.firstLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // choseLabel
            // 
            this.choseLabel.BackColor = System.Drawing.Color.Transparent;
            this.choseLabel.Font = new System.Drawing.Font("Yu Gothic", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.choseLabel.ForeColor = System.Drawing.Color.Fuchsia;
            this.choseLabel.Location = new System.Drawing.Point(12, 98);
            this.choseLabel.Name = "choseLabel";
            this.choseLabel.Size = new System.Drawing.Size(1060, 50);
            this.choseLabel.TabIndex = 1;
            this.choseLabel.Text = "Choose the type of the fractal:";
            this.choseLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // treeButton
            // 
            this.treeButton.BackColor = System.Drawing.Color.Fuchsia;
            this.treeButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.treeButton.FlatAppearance.BorderColor = System.Drawing.Color.Fuchsia;
            this.treeButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.treeButton.Font = new System.Drawing.Font("Yu Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.treeButton.ForeColor = System.Drawing.Color.Black;
            this.treeButton.Image = ((System.Drawing.Image)(resources.GetObject("treeButton.Image")));
            this.treeButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.treeButton.Location = new System.Drawing.Point(152, 165);
            this.treeButton.Name = "treeButton";
            this.treeButton.Size = new System.Drawing.Size(150, 150);
            this.treeButton.TabIndex = 2;
            this.treeButton.Text = "Fractal tree";
            this.treeButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.treeButton.UseVisualStyleBackColor = false;
            this.treeButton.Click += new System.EventHandler(this.TreeButton_Click);
            // 
            // kochButton
            // 
            this.kochButton.AllowDrop = true;
            this.kochButton.BackColor = System.Drawing.Color.Fuchsia;
            this.kochButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.kochButton.FlatAppearance.BorderColor = System.Drawing.Color.Fuchsia;
            this.kochButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.kochButton.Font = new System.Drawing.Font("Yu Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.kochButton.ForeColor = System.Drawing.Color.Black;
            this.kochButton.Image = ((System.Drawing.Image)(resources.GetObject("kochButton.Image")));
            this.kochButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.kochButton.Location = new System.Drawing.Point(475, 165);
            this.kochButton.Name = "kochButton";
            this.kochButton.Size = new System.Drawing.Size(150, 150);
            this.kochButton.TabIndex = 2;
            this.kochButton.Text = "Koch curve";
            this.kochButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.kochButton.UseVisualStyleBackColor = false;
            this.kochButton.Click += new System.EventHandler(this.KochButton_Click);
            // 
            // carpetButton
            // 
            this.carpetButton.AllowDrop = true;
            this.carpetButton.BackColor = System.Drawing.Color.Fuchsia;
            this.carpetButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.carpetButton.FlatAppearance.BorderColor = System.Drawing.Color.Fuchsia;
            this.carpetButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.carpetButton.Font = new System.Drawing.Font("Yu Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.carpetButton.ForeColor = System.Drawing.Color.Black;
            this.carpetButton.Image = ((System.Drawing.Image)(resources.GetObject("carpetButton.Image")));
            this.carpetButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.carpetButton.Location = new System.Drawing.Point(800, 165);
            this.carpetButton.Name = "carpetButton";
            this.carpetButton.Size = new System.Drawing.Size(150, 150);
            this.carpetButton.TabIndex = 2;
            this.carpetButton.Text = "Sierpinski carpet";
            this.carpetButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.carpetButton.UseVisualStyleBackColor = false;
            this.carpetButton.Click += new System.EventHandler(this.CarpetButton_Click);
            // 
            // triangleButton
            // 
            this.triangleButton.AllowDrop = true;
            this.triangleButton.BackColor = System.Drawing.Color.Fuchsia;
            this.triangleButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.triangleButton.FlatAppearance.BorderColor = System.Drawing.Color.Fuchsia;
            this.triangleButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.triangleButton.Font = new System.Drawing.Font("Yu Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.triangleButton.ForeColor = System.Drawing.Color.Black;
            this.triangleButton.Image = ((System.Drawing.Image)(resources.GetObject("triangleButton.Image")));
            this.triangleButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.triangleButton.Location = new System.Drawing.Point(152, 405);
            this.triangleButton.Name = "triangleButton";
            this.triangleButton.Size = new System.Drawing.Size(150, 150);
            this.triangleButton.TabIndex = 2;
            this.triangleButton.Text = "Sierpinski triangle";
            this.triangleButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.triangleButton.UseVisualStyleBackColor = false;
            this.triangleButton.Click += new System.EventHandler(this.TriangleButton_Click);
            // 
            // cuntorButton
            // 
            this.cuntorButton.AllowDrop = true;
            this.cuntorButton.BackColor = System.Drawing.Color.Fuchsia;
            this.cuntorButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.cuntorButton.FlatAppearance.BorderColor = System.Drawing.Color.Fuchsia;
            this.cuntorButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cuntorButton.Font = new System.Drawing.Font("Yu Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.cuntorButton.ForeColor = System.Drawing.Color.Black;
            this.cuntorButton.Image = ((System.Drawing.Image)(resources.GetObject("cuntorButton.Image")));
            this.cuntorButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.cuntorButton.Location = new System.Drawing.Point(475, 405);
            this.cuntorButton.Name = "cuntorButton";
            this.cuntorButton.Size = new System.Drawing.Size(150, 150);
            this.cuntorButton.TabIndex = 2;
            this.cuntorButton.Text = "Cantor set";
            this.cuntorButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cuntorButton.UseVisualStyleBackColor = false;
            this.cuntorButton.Click += new System.EventHandler(this.CuntorButton_Click);
            // 
            // mandelbrotButton
            // 
            this.mandelbrotButton.AllowDrop = true;
            this.mandelbrotButton.BackColor = System.Drawing.Color.Fuchsia;
            this.mandelbrotButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.mandelbrotButton.FlatAppearance.BorderColor = System.Drawing.Color.Fuchsia;
            this.mandelbrotButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.mandelbrotButton.Font = new System.Drawing.Font("Yu Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.mandelbrotButton.ForeColor = System.Drawing.Color.Black;
            this.mandelbrotButton.Image = ((System.Drawing.Image)(resources.GetObject("mandelbrotButton.Image")));
            this.mandelbrotButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.mandelbrotButton.Location = new System.Drawing.Point(800, 405);
            this.mandelbrotButton.Name = "mandelbrotButton";
            this.mandelbrotButton.Size = new System.Drawing.Size(150, 150);
            this.mandelbrotButton.TabIndex = 2;
            this.mandelbrotButton.Text = "Mandelbrot set";
            this.mandelbrotButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.mandelbrotButton.UseVisualStyleBackColor = false;
            this.mandelbrotButton.Click += new System.EventHandler(this.MandelbrotButton_Click);
            // 
            // homeButton
            // 
            this.homeButton.BackColor = System.Drawing.Color.Fuchsia;
            this.homeButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.homeButton.Font = new System.Drawing.Font("Segoe UI", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.homeButton.Image = ((System.Drawing.Image)(resources.GetObject("homeButton.Image")));
            this.homeButton.Location = new System.Drawing.Point(40, 40);
            this.homeButton.Name = "homeButton";
            this.homeButton.Size = new System.Drawing.Size(40, 40);
            this.homeButton.TabIndex = 3;
            this.homeButton.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.homeButton.UseVisualStyleBackColor = false;
            this.homeButton.Visible = false;
            this.homeButton.Click += new System.EventHandler(this.BackButton_Click);
            // 
            // lowStepsPicture
            // 
            this.lowStepsPicture.Image = ((System.Drawing.Image)(resources.GetObject("lowStepsPicture.Image")));
            this.lowStepsPicture.Location = new System.Drawing.Point(152, 98);
            this.lowStepsPicture.Name = "lowStepsPicture";
            this.lowStepsPicture.Size = new System.Drawing.Size(150, 150);
            this.lowStepsPicture.TabIndex = 4;
            this.lowStepsPicture.TabStop = false;
            this.lowStepsPicture.Visible = false;
            // 
            // maxStepsPicture
            // 
            this.maxStepsPicture.Image = ((System.Drawing.Image)(resources.GetObject("maxStepsPicture.Image")));
            this.maxStepsPicture.Location = new System.Drawing.Point(800, 98);
            this.maxStepsPicture.Name = "maxStepsPicture";
            this.maxStepsPicture.Size = new System.Drawing.Size(150, 150);
            this.maxStepsPicture.TabIndex = 5;
            this.maxStepsPicture.TabStop = false;
            this.maxStepsPicture.Visible = false;
            // 
            // stepsBar
            // 
            this.stepsBar.BackColor = System.Drawing.Color.Fuchsia;
            this.stepsBar.Location = new System.Drawing.Point(308, 151);
            this.stepsBar.Maximum = 20;
            this.stepsBar.Minimum = 1;
            this.stepsBar.Name = "stepsBar";
            this.stepsBar.Size = new System.Drawing.Size(486, 45);
            this.stepsBar.TabIndex = 6;
            this.stepsBar.TabStop = false;
            this.stepsBar.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.stepsBar.Value = 1;
            this.stepsBar.Visible = false;
            // 
            // gradientBox
            // 
            this.gradientBox.BackColor = System.Drawing.Color.Fuchsia;
            this.gradientBox.Location = new System.Drawing.Point(308, 342);
            this.gradientBox.Name = "gradientBox";
            this.gradientBox.Size = new System.Drawing.Size(486, 45);
            this.gradientBox.TabIndex = 7;
            this.gradientBox.TabStop = false;
            this.gradientBox.Visible = false;
            // 
            // firstColorButton
            // 
            this.firstColorButton.AllowDrop = true;
            this.firstColorButton.BackColor = System.Drawing.Color.Fuchsia;
            this.firstColorButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.firstColorButton.FlatAppearance.BorderColor = System.Drawing.Color.Fuchsia;
            this.firstColorButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.firstColorButton.Font = new System.Drawing.Font("Yu Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.firstColorButton.ForeColor = System.Drawing.Color.Black;
            this.firstColorButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.firstColorButton.Location = new System.Drawing.Point(152, 288);
            this.firstColorButton.Name = "firstColorButton";
            this.firstColorButton.Size = new System.Drawing.Size(150, 150);
            this.firstColorButton.TabIndex = 2;
            this.firstColorButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.firstColorButton.UseVisualStyleBackColor = false;
            this.firstColorButton.Visible = false;
            this.firstColorButton.Click += new System.EventHandler(this.FirstColorButton_Click);
            // 
            // endColorButton
            // 
            this.endColorButton.AllowDrop = true;
            this.endColorButton.BackColor = System.Drawing.Color.Fuchsia;
            this.endColorButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.endColorButton.FlatAppearance.BorderColor = System.Drawing.Color.Fuchsia;
            this.endColorButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.endColorButton.Font = new System.Drawing.Font("Yu Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.endColorButton.ForeColor = System.Drawing.Color.Black;
            this.endColorButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.endColorButton.Location = new System.Drawing.Point(800, 288);
            this.endColorButton.Name = "endColorButton";
            this.endColorButton.Size = new System.Drawing.Size(150, 150);
            this.endColorButton.TabIndex = 2;
            this.endColorButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.endColorButton.UseVisualStyleBackColor = false;
            this.endColorButton.Visible = false;
            this.endColorButton.Click += new System.EventHandler(this.EndColorButton_Click);
            // 
            // firstColorLabel
            // 
            this.firstColorLabel.AllowDrop = true;
            this.firstColorLabel.AutoSize = true;
            this.firstColorLabel.BackColor = System.Drawing.Color.Fuchsia;
            this.firstColorLabel.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.firstColorLabel.Location = new System.Drawing.Point(164, 415);
            this.firstColorLabel.Name = "firstColorLabel";
            this.firstColorLabel.Size = new System.Drawing.Size(127, 19);
            this.firstColorLabel.TabIndex = 8;
            this.firstColorLabel.Text = "Choose first color";
            this.firstColorLabel.Visible = false;
            // 
            // endColorLabel
            // 
            this.endColorLabel.AllowDrop = true;
            this.endColorLabel.AutoSize = true;
            this.endColorLabel.BackColor = System.Drawing.Color.Fuchsia;
            this.endColorLabel.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.endColorLabel.Location = new System.Drawing.Point(814, 415);
            this.endColorLabel.Name = "endColorLabel";
            this.endColorLabel.Size = new System.Drawing.Size(126, 19);
            this.endColorLabel.TabIndex = 9;
            this.endColorLabel.Text = "Choose end color";
            this.endColorLabel.Visible = false;
            // 
            // gradientLabel
            // 
            this.gradientLabel.AutoSize = true;
            this.gradientLabel.BackColor = System.Drawing.Color.Transparent;
            this.gradientLabel.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.gradientLabel.ForeColor = System.Drawing.Color.Fuchsia;
            this.gradientLabel.Location = new System.Drawing.Point(442, 390);
            this.gradientLabel.Name = "gradientLabel";
            this.gradientLabel.Size = new System.Drawing.Size(221, 37);
            this.gradientLabel.TabIndex = 10;
            this.gradientLabel.Text = "Fractal gradient";
            this.gradientLabel.Visible = false;
            // 
            // stepsLabel
            // 
            this.stepsLabel.BackColor = System.Drawing.Color.Transparent;
            this.stepsLabel.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.stepsLabel.ForeColor = System.Drawing.Color.Fuchsia;
            this.stepsLabel.Location = new System.Drawing.Point(308, 199);
            this.stepsLabel.Name = "stepsLabel";
            this.stepsLabel.Size = new System.Drawing.Size(486, 58);
            this.stepsLabel.TabIndex = 11;
            this.stepsLabel.Text = "Steps count";
            this.stepsLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.stepsLabel.Visible = false;
            // 
            // drawButton
            // 
            this.drawButton.BackColor = System.Drawing.Color.Fuchsia;
            this.drawButton.FlatAppearance.BorderColor = System.Drawing.Color.Fuchsia;
            this.drawButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.drawButton.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.drawButton.Location = new System.Drawing.Point(425, 481);
            this.drawButton.Name = "drawButton";
            this.drawButton.Size = new System.Drawing.Size(264, 74);
            this.drawButton.TabIndex = 12;
            this.drawButton.Text = "Draw!";
            this.drawButton.UseVisualStyleBackColor = false;
            this.drawButton.Visible = false;
            this.drawButton.Click += new System.EventHandler(this.DrawButton_Click);
            // 
            // firstAngleBox
            // 
            this.firstAngleBox.Image = ((System.Drawing.Image)(resources.GetObject("firstAngleBox.Image")));
            this.firstAngleBox.Location = new System.Drawing.Point(46, 284);
            this.firstAngleBox.Name = "firstAngleBox";
            this.firstAngleBox.Size = new System.Drawing.Size(150, 150);
            this.firstAngleBox.TabIndex = 13;
            this.firstAngleBox.TabStop = false;
            this.firstAngleBox.Visible = false;
            // 
            // secondAngleBox
            // 
            this.secondAngleBox.Image = ((System.Drawing.Image)(resources.GetObject("secondAngleBox.Image")));
            this.secondAngleBox.Location = new System.Drawing.Point(539, 284);
            this.secondAngleBox.Name = "secondAngleBox";
            this.secondAngleBox.Size = new System.Drawing.Size(150, 150);
            this.secondAngleBox.TabIndex = 13;
            this.secondAngleBox.TabStop = false;
            this.secondAngleBox.Visible = false;
            // 
            // firstAngleBar
            // 
            this.firstAngleBar.AllowDrop = true;
            this.firstAngleBar.BackColor = System.Drawing.Color.Fuchsia;
            this.firstAngleBar.Location = new System.Drawing.Point(202, 342);
            this.firstAngleBar.Maximum = 100;
            this.firstAngleBar.Minimum = 30;
            this.firstAngleBar.Name = "firstAngleBar";
            this.firstAngleBar.Size = new System.Drawing.Size(331, 45);
            this.firstAngleBar.TabIndex = 6;
            this.firstAngleBar.TabStop = false;
            this.firstAngleBar.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.firstAngleBar.Value = 30;
            this.firstAngleBar.Visible = false;
            // 
            // secondAngleBar
            // 
            this.secondAngleBar.AllowDrop = true;
            this.secondAngleBar.BackColor = System.Drawing.Color.Fuchsia;
            this.secondAngleBar.Location = new System.Drawing.Point(695, 342);
            this.secondAngleBar.Maximum = 100;
            this.secondAngleBar.Minimum = 30;
            this.secondAngleBar.Name = "secondAngleBar";
            this.secondAngleBar.Size = new System.Drawing.Size(331, 45);
            this.secondAngleBar.TabIndex = 6;
            this.secondAngleBar.TabStop = false;
            this.secondAngleBar.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.secondAngleBar.Value = 30;
            this.secondAngleBar.Visible = false;
            // 
            // firstAngleLabel
            // 
            this.firstAngleLabel.AutoSize = true;
            this.firstAngleLabel.BackColor = System.Drawing.Color.Transparent;
            this.firstAngleLabel.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.firstAngleLabel.ForeColor = System.Drawing.Color.Fuchsia;
            this.firstAngleLabel.Location = new System.Drawing.Point(308, 390);
            this.firstAngleLabel.Name = "firstAngleLabel";
            this.firstAngleLabel.Size = new System.Drawing.Size(151, 37);
            this.firstAngleLabel.TabIndex = 14;
            this.firstAngleLabel.Text = "First angle";
            this.firstAngleLabel.Visible = false;
            // 
            // secondAngleLabel
            // 
            this.secondAngleLabel.AutoSize = true;
            this.secondAngleLabel.BackColor = System.Drawing.Color.Transparent;
            this.secondAngleLabel.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.secondAngleLabel.ForeColor = System.Drawing.Color.Fuchsia;
            this.secondAngleLabel.Location = new System.Drawing.Point(778, 390);
            this.secondAngleLabel.Name = "secondAngleLabel";
            this.secondAngleLabel.Size = new System.Drawing.Size(188, 37);
            this.secondAngleLabel.TabIndex = 14;
            this.secondAngleLabel.Text = "Second angle";
            this.secondAngleLabel.Visible = false;
            // 
            // fractalBox
            // 
            this.fractalBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fractalBox.Location = new System.Drawing.Point(0, 0);
            this.fractalBox.Name = "fractalBox";
            this.fractalBox.Size = new System.Drawing.Size(1084, 601);
            this.fractalBox.TabIndex = 15;
            this.fractalBox.TabStop = false;
            this.fractalBox.Visible = false;
            // 
            // MainForm
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1084, 601);
            this.Controls.Add(this.homeButton);
            this.Controls.Add(this.fractalBox);
            this.Controls.Add(this.secondAngleLabel);
            this.Controls.Add(this.firstAngleLabel);
            this.Controls.Add(this.secondAngleBar);
            this.Controls.Add(this.firstAngleBar);
            this.Controls.Add(this.secondAngleBox);
            this.Controls.Add(this.firstAngleBox);
            this.Controls.Add(this.drawButton);
            this.Controls.Add(this.stepsLabel);
            this.Controls.Add(this.gradientLabel);
            this.Controls.Add(this.endColorLabel);
            this.Controls.Add(this.firstColorLabel);
            this.Controls.Add(this.gradientBox);
            this.Controls.Add(this.stepsBar);
            this.Controls.Add(this.maxStepsPicture);
            this.Controls.Add(this.lowStepsPicture);
            this.Controls.Add(this.mandelbrotButton);
            this.Controls.Add(this.cuntorButton);
            this.Controls.Add(this.triangleButton);
            this.Controls.Add(this.carpetButton);
            this.Controls.Add(this.kochButton);
            this.Controls.Add(this.treeButton);
            this.Controls.Add(this.choseLabel);
            this.Controls.Add(this.firstLabel);
            this.Controls.Add(this.firstColorButton);
            this.Controls.Add(this.endColorButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "MainForm";
            this.Text = "DiveIntoTheFractal";
            this.Resize += new System.EventHandler(this.MainFormResize);
            ((System.ComponentModel.ISupportInitialize)(this.lowStepsPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxStepsPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stepsBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gradientBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstAngleBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondAngleBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstAngleBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondAngleBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fractalBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label firstLabel;
        private System.Windows.Forms.Label choseLabel;
        private System.Windows.Forms.Button treeButton;
        private System.Windows.Forms.Button kochButton;
        private System.Windows.Forms.Button carpetButton;
        private System.Windows.Forms.Button triangleButton;
        private System.Windows.Forms.Button cuntorButton;
        private System.Windows.Forms.Button mandelbrotButton;
        private System.Windows.Forms.Button homeButton;
        private System.Windows.Forms.PictureBox lowStepsPicture;
        private System.Windows.Forms.PictureBox maxStepsPicture;
        private System.Windows.Forms.ColorDialog firstColorDiolog;
        private System.Windows.Forms.ColorDialog endColorDiolog;
        private System.Windows.Forms.TrackBar stepsBar;
        private System.Windows.Forms.PictureBox gradientBox;
        private System.Windows.Forms.Button firstColorButton;
        private System.Windows.Forms.Button endColorButton;
        private System.Windows.Forms.Label firstColorLabel;
        private System.Windows.Forms.Label endColorLabel;
        private System.Windows.Forms.Label gradientLabel;
        private System.Windows.Forms.Label stepsLabel;
        private System.Windows.Forms.Button drawButton;
        private System.Windows.Forms.PictureBox firstAngleBox;
        private System.Windows.Forms.PictureBox secondAngleBox;
        private System.Windows.Forms.TrackBar firstAngleBar;
        private System.Windows.Forms.TrackBar secondAngleBar;
        private System.Windows.Forms.Label firstAngleLabel;
        private System.Windows.Forms.Label secondAngleLabel;
        private System.Windows.Forms.PictureBox fractalBox;
    }
}

