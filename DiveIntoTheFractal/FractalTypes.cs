﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using static System.Math;

namespace DiveIntoTheFractal
{
    class TreeFractal : Fractal
    {
        public double firstAngle;
        public double secondAngle;
        public double segmentCoefficient;
        public override void Draw()
        {
            segmentLength = fractalBox.Height / 3;
            DrawSegment(new Point(fractalBox.Width / 2, fractalBox.Height), segmentLength, PI / 2, 0);
        }

        private void DrawSegment(Point currentPoint, int currentLength, double currentAngle, int step)
        {
            if (step == stepsCount)
            {
                return;
            }
            var nextPoint = new Point(currentPoint.X + (int)(currentLength * Cos(currentAngle)), currentPoint.Y - (int)(currentLength * Sin(currentAngle)));
            Pen firstPen = new Pen(Color.FromArgb((int)((double)step / stepsCount * 255), firstColor));
            fractalGraphics.DrawLine(firstPen, currentPoint, nextPoint);
            Pen endPen = new Pen(Color.FromArgb((int)(255 - (double)step / stepsCount * 255), endColor));
            fractalGraphics.DrawLine(endPen, currentPoint, nextPoint);
            DrawSegment(nextPoint, (int)(currentLength * segmentCoefficient), currentAngle - firstAngle, step + 1);
            DrawSegment(nextPoint, (int)(currentLength * segmentCoefficient), currentAngle + secondAngle, step + 1);
        }
    }

    class KochFractal : Fractal
    {
        public override void Draw()
        {
            var firstPoint = new Point(fractalBox.Width / 3, fractalBox.Height / 2);
            var secondPoint = new Point(2 * fractalBox.Width / 3, fractalBox.Height / 2);
            Pen firstPen = new Pen(firstColor);
            fractalGraphics.DrawLine(firstPen, firstPoint, secondPoint);
            var thirdPoint = new Point(fractalBox.Width / 2, fractalBox.Height / 2 + (int)(Sqrt(3) / 2 * (secondPoint.X - firstPoint.X)));
            Draw(firstPoint, secondPoint, thirdPoint, 0);
        }

        private void Draw(Point firstPoint, Point secondPoint, Point thirdPoint, int step)
        {
            if (step == stepsCount)
            {
                return;
            }
            var centre = new Point((firstPoint.X + secondPoint.X + thirdPoint.X) / 3, (firstPoint.Y + secondPoint.Y + thirdPoint.Y) / 3);
            var nextFirstPoint = new Point(centre.X + (centre.X - thirdPoint.X), centre.Y + (centre.Y - thirdPoint.Y));
            var nextSecondPoint = new Point(firstPoint.X + (secondPoint.X - firstPoint.X) / 3,
                                            firstPoint.Y + (secondPoint.Y - firstPoint.Y) / 3);
            var nextThirdPoint = new Point(firstPoint.X + 2 * (secondPoint.X - firstPoint.X) / 3,
                                            firstPoint.Y + 2 * (secondPoint.Y - firstPoint.Y) / 3);
            var whitePen = new Pen(Color.White, 2);
            fractalGraphics.DrawLine(whitePen, nextSecondPoint, nextThirdPoint);
            Pen firstPen = new Pen(Color.FromArgb((int)((double)step / stepsCount * 255), firstColor));
            fractalGraphics.DrawLine(firstPen, nextFirstPoint, nextSecondPoint);
            fractalGraphics.DrawLine(firstPen, nextFirstPoint, nextThirdPoint);
            Pen endPen = new Pen(Color.FromArgb((int)(255 - (double)step / stepsCount * 255), endColor));
            fractalGraphics.DrawLine(endPen, nextFirstPoint, nextSecondPoint);
            fractalGraphics.DrawLine(endPen, nextFirstPoint, nextThirdPoint);
            Draw(nextFirstPoint, nextSecondPoint, nextThirdPoint, step + 1);
            Draw(nextFirstPoint, nextThirdPoint, nextSecondPoint, step + 1);
        }
    }

    class CarpetFractal : Fractal
    {
        public override void Draw()
        {
            Draw(new Point(fractalBox.Width / 4, 0), fractalBox.Width / 2);
        }

        private void Draw(Point leftUpCorner, int size, int step = 0)
        {
            if (step == stepsCount)
            {
                return;
            }
            SolidBrush firstBrush = new SolidBrush(Color.FromArgb((int)((double)step / stepsCount * 255), firstColor));
            fractalGraphics.FillRectangle(firstBrush, leftUpCorner.X, leftUpCorner.Y, size, size);
            SolidBrush endBrush = new SolidBrush(Color.FromArgb((int)(255 - (double)step / stepsCount * 255), endColor));
            fractalGraphics.FillRectangle(endBrush, leftUpCorner.X, leftUpCorner.Y, size, size);
            size /= 3;
            for (int i = leftUpCorner.X; i < leftUpCorner.X + 3 * size; i += size)
            {
                for (int j = leftUpCorner.Y; j < leftUpCorner.Y + 3 * size; j += size)
                {
                    if (i != 1 || j != 1)
                    {
                        Draw(new Point(i, j), size, step + 1);
                    }
                }
            }
            SolidBrush whiteBrush = new SolidBrush(Color.White);
            fractalGraphics.FillRectangle(whiteBrush, leftUpCorner.X + size, leftUpCorner.Y + size, size, size);
        }
    }
    class TriangleFractal : Fractal
    {
        public override void Draw()
        {
            var firstPoint = new Point(fractalBox.Width / 3, fractalBox.Height / 3);
            var secondPoint = new Point(2 * fractalBox.Width / 3, fractalBox.Height / 3);
            var thirdPoint = new Point(fractalBox.Width / 2, fractalBox.Height / 3 + (int)(Sqrt(3) / 2 * (secondPoint.X - firstPoint.X)));
            Draw(firstPoint, secondPoint, thirdPoint);
        }

        private void Draw(Point firstPoint, Point secondPoint, Point thirdPoint, int step = 0)
        {
            if (step == stepsCount)
            {
                return;
            }
            Pen firstPen = new Pen(Color.FromArgb((int)((double)step / stepsCount * 255), firstColor));
            fractalGraphics.DrawLine(firstPen, firstPoint, secondPoint);
            fractalGraphics.DrawLine(firstPen, secondPoint, thirdPoint);
            fractalGraphics.DrawLine(firstPen, thirdPoint, firstPoint);
            Pen endPen = new Pen(Color.FromArgb((int)(255 - (double)step / stepsCount * 255), endColor));
            fractalGraphics.DrawLine(endPen, firstPoint, secondPoint);
            fractalGraphics.DrawLine(endPen, secondPoint, thirdPoint);
            fractalGraphics.DrawLine(endPen, thirdPoint, firstPoint);
            var nextFirstPoint = new Point((firstPoint.X + secondPoint.X) / 2, (firstPoint.Y + secondPoint.Y) / 2);
            var nextSecondPoint = new Point((thirdPoint.X + secondPoint.X) / 2, (thirdPoint.Y + secondPoint.Y) / 2);
            var nextThirdPoint = new Point((firstPoint.X + thirdPoint.X) / 2, (thirdPoint.Y + firstPoint.Y) / 2);
            Draw(firstPoint, nextFirstPoint, nextThirdPoint, step + 1);
            Draw(nextFirstPoint, secondPoint, nextSecondPoint, step + 1);
            Draw(nextSecondPoint, thirdPoint, nextThirdPoint, step + 1);
        }
    }
    class CuntorFractal : Fractal
    {
        public int segmentDistance;
        public override void Draw()
        {
            var firstPoint = new Point(fractalBox.Width / 3, fractalBox.Height / 2);
            var secondPoint = new Point(2 * fractalBox.Width / 3, fractalBox.Height / 2);
            Draw(firstPoint, secondPoint);
        }

        private void Draw(Point firstPoint, Point secondPoint, int step = 0)
        {
            if (step == stepsCount)
            {
                return;
            }
            Pen firstPen = new Pen(Color.FromArgb((int)((double)step / stepsCount * 255), firstColor), 5);
            fractalGraphics.DrawLine(firstPen, firstPoint, secondPoint);
            Pen endPen = new Pen(Color.FromArgb((int)(255 - (double)step / stepsCount * 255), endColor), 5);
            fractalGraphics.DrawLine(endPen, firstPoint, secondPoint);
            var nextFirstPoint = new Point(firstPoint.X + (secondPoint.X - firstPoint.X) / 3, firstPoint.Y + segmentDistance);
            var nextSecondPoint = new Point(firstPoint.X + 2 * (secondPoint.X - firstPoint.X) / 3, firstPoint.Y + segmentDistance);
            firstPoint.Y += segmentDistance;
            secondPoint.Y += segmentDistance;
            Draw(firstPoint, nextFirstPoint, step + 1);
            Draw(nextSecondPoint, secondPoint, step + 1);
        }
    }
    class MandelbrotFractal : Fractal
    {
        public override void Draw()
        {
            double scalex = 0.02;
            double scaley = 0.02;
            int Width = fractalBox.Width;
            int Height = fractalBox.Height;
            int dx = Width / 2;
            int dy = Height / 2;
            int max = 10;
            int coef1 = 5;
            int coef2 = 88;
            int i = 1;
            for (int ix = 0; ix < Width - 1; ++ix)
            {
                for (int iy = 0; iy < Height - 1; ++iy)
                {
                    double x = 0.0;
                    double y = 0.0;
                    double cx = scalex * (ix - dx);
                    double cy = scaley * (iy - dy);
                    i = 1;
                    while (i < 255)
                    {
                        double x1 = x * x - y * y + cx;
                        double y1 = 2 * x * y + cy;
                        x = x1;
                        y = y1;
                        if ((Abs(x) > max) && (Abs(y) > max))
                        {
                            break;
                        }
                        i += 1;
                    }
                    ((Bitmap)fractalBox.Image).SetPixel(ix, iy, Color.FromArgb(255, 255, Max(0, 255 - 20 * i), Max(0, 255 - 20 * i)));
                }
            }
        }
    }
}
