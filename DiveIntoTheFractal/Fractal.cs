﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace DiveIntoTheFractal
{
    public class Fractal
    {
        public int stepsCount;
        public Color firstColor;
        public Color endColor;
        public int segmentLength = 100;
        public Graphics fractalGraphics;
        public PictureBox fractalBox;

        virtual public void Draw() { }
    }
}
